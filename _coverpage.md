# Mmdz’s Blog
## 热爱生活，热爱自己
日常随记 | 点滴生活 | 技术博客

<center><font face="华文行楷" style="color: rgb(116, 126, 221);font-size: 18px;">春有百花秋有月，夏有凉风冬有雪。</font></center>
<center><font face="华文行楷" style="color: rgb(116, 126, 221);font-size: 18px;">若无闲事挂心头，便是人间好时节。</font></center>
<div style="font-weight: 650;text-align: right;color: rgb(116, 126, 221);font-size: 15px;margin-right: 32%;"><font face="宋体">———《颂》</font></div>

[官方文档](https://docsify.js.org/#/)
[Gitee](https://github.com/mmdz-blog)
[Getting Started](#main)

<!-- background image -->

![](_media/02.jpeg)

