# Soybean Admin

### 项目介绍

> Soybean Admin 使用的是Vue3作为前端框架，TypeScript作为开发语言，同时还整合了NaiveUI组件库，使得系统具有高可用性和用户友好性。
>
> ![](imgs\Soybean_01.png)

### 性能特色

> - 采用最新流行的前端技术栈，如Vue3/Vite，并使用高效的npm包管理器pnpm。
> - TypeScript：一种用于开发应用程序级JavaScript的语言。
> - 主题：提供丰富可配置的主题选项，包括暗黑模式，基于原子CSS框架-UnoCss的动态主题颜色。
> - 代码规范：提供丰富的规范插件和极高的代码规范标准。
> - 文件路由系统：基于文件的路由系统，根据页面文件自动生成路由声明、导入和模块。
> - 权限路由：支持前端静态和后端动态两种路由模式，并通过基于mock的动态路由快速实现后端动态路由。
> - 请求函数：对axios进行了完善的封装，提供了Promise和hooks两种请求函数，并加入了请求结果数据转换的适配器。

### 项目资料

> 项目文档：**https://docs.soybean.pro/zh/guide/introduction.html**
>
> 项目地址：**https://github.com/honghuangdc/soybean-admin**