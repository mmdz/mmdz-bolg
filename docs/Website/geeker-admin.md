# Geeker Admin

### 项目介绍

> 这是一个基于 Vue3.4、TypeScript、Vite5、Pinia、Element-Plus 开源的后台管理框架，可以帮助我们快速构建功能强大、界面美观、易于使用的后台管理系统。
>
> ![](imgs\Geeker-Admin_01.jpg)

### 性能特色

> - 技术栈领先：基于 Vue 3、TypeScript、Vite 和 Element Plus 等最新技术栈开发，性能强劲，开发体验流畅。
> - 功能丰富：开箱即用，提供权限管理、多主题、国际化、菜单管理、数据表格、表单验证、图表展示等常用功能，满足你的日常开发需求。
> - 易于使用：代码结构清晰，文档详细，上手难度低，即使是初学者也可以快速使用。
> - 高度可定制：支持主题定制、组件扩展、权限控制等，满足你的个性化需求。

### 项目资料

> 项目文档：**https://docs.spicyboy.cn/**
>
> 项目地址：**https://github.com/HalseySpicy/Geeker-Admin**