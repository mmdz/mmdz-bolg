# PMP学习

?>`2023年3月PMP®网络班（映雪班）`

PMP考点笔记总结

> [我的PMP考点笔记总汇](https://www.processon.com/mindmap/639a787d1efad465c9caf19d)

PMP的参考资料

- <a href="/docs/LCE/PMP/pdf/999-PMP各种文件模板参考大全.pdf" target="_blank">999-PMP各种文件模板参考大全</a>
- <a href="/docs/LCE/PMP/pdf/预测部分思维导图.pdf" target="_blank">预测部分思维导图</a>

习题

- <a href="/docs/LCE/PMP/pdf/习题-项目管理基础-项目经理角色-项目运行环境.pdf" target="_blank">习题-项目管理基础-项目经理角色-项目运行环境</a>
- <a href="/docs/LCE/PMP/pdf/习题-项目整合管理.pdf" target="_blank">习题-项目整合管理</a>
- <a href="/docs/LCE/PMP/pdf/习题-项目范围管理.pdf" target="_blank">习题-项目范围管理</a>
- <a href="/docs/LCE/PMP/pdf/习题-项目进度管理.pdf" target="_blank">习题-项目进度管理</a>
- <a href="/docs/LCE/PMP/pdf/习题-项目成本管理.pdf" target="_blank">习题-项目成本管理</a>
- <a href="/docs/LCE/PMP/pdf/习题-项目质量管理.pdf" target="_blank">习题-项目质量管理</a>
- <a href="/docs/LCE/PMP/pdf/习题-项目资源管理.pdf" target="_blank">习题-项目资源管理</a>
- <a href="/docs/LCE/PMP/pdf/习题-项目沟通管理.pdf" target="_blank">习题-项目沟通管理</a>
- <a href="/docs/LCE/PMP/pdf/习题-项目风险管理.pdf" target="_blank">习题-项目风险管理</a>
- <a href="/docs/LCE/PMP/pdf/习题-项目采购管理.pdf" target="_blank">习题-项目采购管理</a>
- <a href="/docs/LCE/PMP/pdf/习题-项目相关方管理.pdf" target="_blank">习题-项目相关方管理</a>
- 敏捷
  - <a href="/docs/LCE/PMP/pdf/习题01-敏捷.pdf" target="_blank">习题01-敏捷</a>
  - <a href="/docs/LCE/PMP/pdf/习题02-SCRUM实践.pdf" target="_blank">习题02-SCRUM实践</a>
  - <a href="/docs/LCE/PMP/pdf/习题03-敏捷.pdf" target="_blank">习题03-敏捷</a>
  - <a href="/docs/LCE/PMP/pdf/习题04-敏捷.pdf" target="_blank">习题04-敏捷</a>


PMP的课件资料

- <a href="/docs/LCE/PMP/pdf/项目管理基础.pdf" target="_blank">项目管理基础</a>
- <a href="/docs/LCE/PMP/pdf/项目运行环境.pdf" target="_blank">项目运行环境</a>
- <a href="/docs/LCE/PMP/pdf/项目经理角色.pdf" target="_blank">项目经理角色</a>
- 项目整合管理⭐
  - <a href="/docs/LCE/PMP/pdf/项目整合管理上.pdf" target="_blank">项目整合管理上</a>
  - <a href="/docs/LCE/PMP/pdf/项目整合管理下.pdf" target="_blank">项目整合管理下</a>
- <a href="/docs/LCE/PMP/pdf/项目范围管理.pdf" target="_blank">项目范围管理</a>
- <a href="/docs/LCE/PMP/pdf/项目进度管理.pdf" target="_blank">项目进度管理</a>
- <a href="/docs/LCE/PMP/pdf/项目成本管理.pdf" target="_blank">项目成本管理</a>
- <a href="/docs/LCE/PMP/pdf/项目质量管理.pdf" target="_blank">项目质量管理</a>
- <a href="/docs/LCE/PMP/pdf/项目资源管理.pdf" target="_blank">项目资源管理⭐</a>
- <a href="/docs/LCE/PMP/pdf/项目沟通管理.pdf" target="_blank">项目沟通管理⭐</a>
- <a href="/docs/LCE/PMP/pdf/项目风险管理.pdf" target="_blank">项目风险管理</a>
- <a href="/docs/LCE/PMP/pdf/项目采购管理.pdf" target="_blank">项目采购管理</a>
- <a href="/docs/LCE/PMP/pdf/项目相关方管理.pdf" target="_blank">项目相关方管理</a>
- 敏捷项目管理
  - <a href="/docs/LCE/PMP/pdf/敏捷的定义.pdf" target="_blank">敏捷的定义</a>
  - <a href="/docs/LCE/PMP/pdf/生命周期选择.pdf" target="_blank">生命周期选择</a>
  - <a href="/docs/LCE/PMP/pdf/敏捷Scrum实践方法.pdf" target="_blank">敏捷Scrum实践方法</a>
  - <a href="/docs/LCE/PMP/pdf/敏捷项目管理阶段框架.pdf" target="_blank">敏捷项目管理阶段框架</a>
  - <a href="/docs/LCE/PMP/pdf/其他敏捷实践.pdf" target="_blank">其他敏捷实践</a>
  - <a href="/docs/LCE/PMP/pdf/敏捷变革模型.pdf" target="_blank">敏捷变革模型</a>
  - <a href="/docs/LCE/PMP/pdf/敏捷部分.pdf" target="_blank">敏捷部分</a>
  - <a href="/docs/LCE/PMP/pdf/预测部分.pdf" target="_blank">预测部分</a>
