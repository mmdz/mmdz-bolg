# 常用工具

图片

------

?>常用网站

<!-- tabs:start -->

#### **电子书**

```
### 图灵社区
- 地 址：https://www.ituring.com.cn/
- 简 介：书籍比较全面的图书社区，电子书的价格是纸质书的一半。

### 博文视点 
- 地 址：http://www.broadview.com.cn/
- 简 介：博文视点也是有一些好书的。

### 书栈网
- 地 址：https://www.bookstack.cn/
- 简 介：一个开源书籍和文档分享站点。

### 计算机书籍控
- 地 址：http://bestcbooks.com/ 
- 简 介：

### it熊猫
- 地 址：https://itpanda.net/book/ 
- 简 介：
```

#### **国内博客社区**

```
### CSDN
- 地 址：https://blog.csdn.net/
- 简 介：最大的中文技术博客社区，内容较多较杂。

### 博客园
- 地 址：https://www.cnblogs.com/
- 简 介：老牌中文技术博客社区，商业化气息要淡一些，之前广为诟病的UI风格也在今年进行了改进，自定义主题也让能让博客百花争艳。

### 简书
- 地 址：https://www.jianshu.com/
- 简 介：并不是纯粹的技术博客社区，很多伤春悲秋的东西。

### 思否
- 地 址：https://segmentfault.com/
- 简 介：包含博客、问答的技术社区。

### 开源中国
- 地 址：https://www.oschina.net
- 简 介：一个技术博客社区。

### 51CTO
- 地 址: https://www.51cto.com/
- 简 介：一个IT技术网站。

### V2EX
- 地 址: https://www.v2ex.com
- 简 介：页面风格很朴素，13格很高。

### 腾讯云社区
- 地 址: https://cloud.tencent.com/developer
- 简 介：腾讯云的开发者社区。

### 阿里云社区
- 地 址: https://yq.aliyun.com
- 简 介：阿里云的开发者社区。

### 开发者头条
- 地 址: https://toutiao.io/
- 简 介：一个程序员分享平台。

### GitChat
- 地 址:https://gitbook.cn/
- 简 介：一个技术博客社区

### 知乎
- 地 址: https://www.zhihu.com/
- 简 介：知乎是个综合性的问答社区，但是聚集的程序员也比较多，有一些高质量的问答和专栏。
```

#### **国外技术博客社区**

```
### Stack Overflow
- 地 址: https://stackoverflow.com/
- 简 介：全球最活跃的程序员技术问答交流社区，有人说程序员的所有问题都能在上面找到答案。

### DEV
- 地 址: https://dev.to/
- 简 介：不错的技术社区。

### DZone
- 地 址: https://dzone.com/
- 简 介：http://DZone.com是世界上最大的在线社区之一。

### Bytes
- 地 址: https://bytes.com/
- 简 介：一个面向开发人员和IT专业人员的交流社区。

### Google Developers
- 地 址: https://developers.google.com/
- 简 介：google开发社区。
```

#### **小微型博客**

```
### 美团技术团队 
- 地 址: https://tech.meituan.com/
- 简 介：美团技术团队的博客，干货满满。

### 阮一峰的网络日志
- 地 址: http://www.ruanyifeng.com/blog/
- 简 介：大神阮一峰，博客风格真正做到深入浅出。

### Web前端导航
- 地 址: http://www.alloyteam.com/nav/
- 简 介：比较全的Web前端导航，包括 团队组织 、开发社区 、 前端门户、框架类库 等等网站的导航。

### 廖雪峰的官方网站
- 地 址: https://www.liaoxuefeng.com/
- 简 介：廖雪峰老师的网站，有一些不错的入门教程。

### 酷壳
- 地 址: https://coolshell.cn/
- 简 介：可以了解陈皓，是个很有个性的人。

### 人工智能社区
- 地 址: https://www.captainbed.net/blog-neo/
- 简 介：不搞人工智能也可以看看，写的很有意思的教程，可以作为科普看看。
```

#### **开源社区**

```
### GitHub
- 地 址: https://github.com/
- 简 介：全球最大开源社区，被戏称为全球最大同性交友网站。
- 每日推荐一个有趣的 GitHub 项目，可以关注公众号：逛逛GitHub

### 码云
- 地 址: https://gitee.com/
- 简 介：可以看做GitHub的国内版，码云是个不错的替代者。
```

#### **面试刷题**

```
### LeetCode
- 地 址: https://leetcode-cn.com/
- 简 介：经典的刷题网站，主要是算法题。

### LintCode
- 地 址: https://www.lintcode.com/
- 简 介：和LeetCode类似

### 牛客网
- 地 址: https://www.nowcoder.com/
- 简 介：一个联网求职学习交流社区。
```

<!-- tabs:end -->