# 树结构

## 树的种类

- **无序树**：树中任意节点的子节点之间没有顺序关系，这种树称为无序树，也称为自由树；
- **有序树**：树中任意节点的子节点之间有顺序关系，这种树称为有序树； 　
- **二叉树**：每个节点最多含有两个子树的树称为二叉树； 　　
- **完全二叉树**：对于一颗二叉树，假设其深度为d（d>1）。除了第d层外，其它各层的节点数目均已达最大值，且第d层所有节点从左向右连续地紧密排列，这样的二叉树被称为完全二叉树； 　　　
- **满二叉树**：所有叶节点都在最底层的完全二叉树； 　　
- **平衡二叉树**（AVL树）：当且仅当任何节点的两棵子树的高度差不大于1的二叉树； 　　
- **排序二叉树**(二叉查找树（英语：Binary Search Tree))：也称二叉搜索树、有序二叉树； 　
- **霍夫曼树**：带权路径最短的二叉树称为哈夫曼树或最优二叉树； 　　
- **B树**：一种对读写操作进行优化的自平衡的二叉查找树，能够保持数据有序，拥有多于两个子树。

## 树的递归遍历

我们利用三种遍历的特点，根据他们的打印顺序总结出来:

- 前序：根左右
- 中序：左跟右
- 后序：左右跟

```java
public class Solution {
    public List<Integer> list = new ArrayList<>();
    //1.前序遍历
    public void preorder(TreeNode root){
        if(root != null){
            list.add(root.val);
            preorder(root.left);
            preorder(root.right);
        }
    }
    //2.中序遍历,递归实现
    public void inorder1(TreeNode root){
        if(root != null){
            inorder(root.left);
            list.add(root.val);
            inorder(root.right);
        }
    }
    //3.后序遍历
    public void backorder(TreeNode root){
        if(root != null){
            backorder(root.left);
            backorder(root.right);
            list.add(root.val);
        }
    }
}
```

## 常见树的特点

相关术语

- 根节点：最顶层的节点就是根结点，它是整棵树的源头
- 叶子节点：在树下端的节点，就是其子节点个数为0的节点
- 节点的度：指定节点有几个分叉就说这个节点的度是几
- 树的度：只看根结点，树的度等价于根节点的度
- 节点高度：指从这个节点到叶子节点的距离（一共经历了几个节点）
- 节点深度：指从这个节点到根节点的距离（一共经历了几个节点）
- 树的高度：指所有节点高度的最大值
- 树的深度：指所有节点深度的最大值
- 节点的层：从根节点开始，假设根节点为第1层，根节点的子节点为第2层，依此类推

### 二叉树

> 二叉树是对普通树形结构进行限定得到的一种特殊的树，规定树中节点的度不大于2，当节点有两个子节点，也就是有两颗子树时，它们有左右之分，分别被称为左子树和右子树，左子树和右子树又同样都是二叉树。
>
> 完美二叉树（Perfect Binary Tree）：除了叶子结点之外的每一个结点都有两个孩子，每一层都被完全填充
>
> ![](other_imgs\383bdc794a30f33b0372647cbb986123.png)
>
> 完全二叉树（Complete Binary Tree）：除了最后一层之外的其他每一层都被完全填充，并且所有结点都保持向左对齐
>
> ![](other_imgs\1908821df6729563662a810b551480eb.png)
>
>
> 完满二叉树（Full Binary Tree）： 除了叶子结点之外的每一个结点都有两个孩子结点
>
> ![](other_imgs\c421a0eb19ed4cb3280007ca5f478cb2.png)
>

### 二叉查找树

二叉查找树是一种特殊的二叉树，又称为排序二叉树、二叉搜索树、二叉排序树等等，它实际上是数据域有序的二叉树，即对树上的每个结点，都满足其左子树上所有结点的数据域均小于或等于根结点的数据域，右子树上所有结点的数据域均大于根结点的数据域。

**红黑树**

> 红黑树也是一颗二叉查找树，需要为每个节点存储节点的颜色，可以是红或黑。通过对任何一条从根到叶子的路径上各个节点着色的方式的限制，来确保没有一条路径会比其它路径长出两倍，因此，红黑树是一种弱平衡二叉树。
>
> 由于是弱平衡二叉树，那么在相同的节点情况下，AVL树的高度小于等于红黑树的高度，相对于要求严格的AVL树来说，它的旋转次数少，所以对于插入，删除操作较多的情况下，用红黑树的查找效率会更高一些。
>
> **特点**
>
> 1. 每个节点非红即黑
> 2. 根节点是黑的
> 3. 每个叶子节点（叶子节点即树尾端NULL节点）都是黑的
> 4. 每条路径都包含相同的黑节点
> 5. 如果一个节点是红的，那么它的两儿子都是黑的
> 6. 对于任意节点而言，其到叶子点的每条路径都包含相同数目的黑节点

**B树**

> B树是一个多路平衡查找树，B树的出现是为了弥合不同的存储级别之间的访问速度上的巨大差异，实现高效的I/O。平衡二叉树的查找效率是非常高的，并可以通过降低树的深度来提高查找的效率。但是当数据量非常大，树的存储的元素数量是有限的，这样会导致二叉查找树结构由于树的深度过大而造成磁盘I/O读写过于频繁，进而导致查询效率低下，同时数据量过大会导致内存空间不够容纳平衡二叉树所有结点的情况，而B树是解决这个问题的很好的结构。
>
> 要想了解B树需要了解一个很重要的概念，B树中所有节点的度的最大值称为B树的阶，记为m，这是一个跟重要值，也就是说m阶B树指的是节点度最大为m的B树。
>
> 定义及特点
>
> - 每个节点最多只有m个子节点
> - 根结点的儿子数为[2, m]
> - 除根结点以外的非叶子结点的儿子数为[m/2, m]，向上取整
> - 非叶子结点的关键字个数=子节点数-1
> - 所有叶子都出现在同一层
> - k个关键字把节点拆成k+1段，分别指向k+1个儿子，同时满足查找树的大小关系
> - 非叶子节点中不仅包含索引，也会包含数据
>
> 应用
>
> B树是一种平衡的多路查找树，主要用作文件的索引。其优势是当你要查找的值恰好处在一个非叶子节点时，查找到该节点就会成功并结束查询，有很多基于频率的搜索是选用B树，越频繁查询的结点越往根上走，前提是需要对查询做统计，而且要对key做一些变化。

**B+树**

> B+树是b树的一种变体，查询性能更好，m阶的b+树具有以下特征：
>
> - 有n棵子树的非叶子结点中含有n个关键字（b树是n-1个），这些关键字不保存数据，只用来索引，所有数据都保存在叶子节点（b树是每个关键字都保存数据）
> - 所有的叶子结点中包含了全部关键字的信息，及指向含这些关键字记录的指针，且叶子结点本身依关键字的大小自小而大顺序链接
> - 所有的非叶子结点可以看成是索引部分，结点中仅含其子树中的最大（或最小）关键字
> - 通常在b+树上有两个头指针，一个指向根结点，一个指向关键字最小的叶子结点
> - 同一个数字会在不同节点中重复出现，根节点的最大元素就是b+树的最大元素
>
> B+树的优势及应用
>
> - B+tree的内部结点并没有指向关键字具体信息的指针。因此其内部结点相对B树更小。如果把所有同一内部结点的关键字存放在同一盘块中，那么盘块所能容纳的关键字数量也越多。一次性读入内存中的需要查找的关键字也就越多，相对来说IO读写次数也就降低了。
> - 由于非叶子结点并不是最终指向文件内容的结点，而只是叶子结点中关键字的索引。所以任何关键字的查找必须走一条从根结点到叶子结点的路。所有关键字查询的路径长度相同，导致每一个数据的查询效率相当。
>
> - B+树支持范围遍历，只要遍历叶子节点就可以实现整棵树的遍历，而在数据库中基于范围的查询是非常频繁的，这一点要明显由于B树。