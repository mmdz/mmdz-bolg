# Rocketmq

### 为什么要使用MQ？

因为项目比较大，做了分布式系统，所有远程服务调用请求都是**同步执行**经常出问题，所以引入了mq

| 作用 | 描述                                                         |
| ---- | ------------------------------------------------------------ |
| 解耦 | 系统耦合度降低，没有强依赖关系                               |
| 异步 | 不需要同步执行的远程调用可以有效提高响应时间                 |
| 削峰 | 请求达到峰值后，后端service还可以保持固定消费速率消费，不会被压垮 |

### 消息队列带来的一系列问题(消息丢失、消息堆积、重复消费/消息幂等、顺序消费、消息延迟消费、分布式事务等等)⭐

<!-- tabs:start -->

#### **消息丢失**

> 消息丢失可能发生在生产者发送消息、MQ本身丢失消息、消费者丢失消息3个方面。
>
> ![](rocketmq_imgs\Zy82NDA.png)
>
> - **生产者丢失**
>
>   **原因：**产者将消息发送给Rocket MQ的时候，如果出现了网络抖动或者通信异常等问题，消息就有可能会丢失。
>
>   **解决：**
>
>   保证消息不丢失的方案是使用RocketMQ自带的事务机制来发送消息，大致流程为：
>
>   - 首先生产者发送half消息到RocketMQ中，此时消费者是无法消费half消息的，若half消息就发送失败了，则执行相应的回滚逻辑
>   - half消息发送成功之后，且RocketMQ返回成功响应，则执行生产者的核心链路
>   - 如果生产者自己的核心链路执行失败，则回滚，并通知RocketMQ删除half消息
>   - 如果生产者的核心链路执行成功，则通知RocketMQ commit half消息，让消费者可以消费这条数据
>
>   在使用了RocketMQ事务将生产者的消息成功发送给RocketMQ，就可以保证在这个阶段消息不会丢失
>
> - **MQ丢失**
>
>   **原因：**消息需要持久化到磁盘中，这时会有两种情况导致消息丢失
>
>   - RocketMQ为了减少磁盘的IO，会先将消息写入到os cache中，而不是直接写入到磁盘中，消费者从os cache中获取消息类似于直接从内存中获取消息，速度更快，过一段时间会由os线程异步的将消息刷入磁盘中，此时才算真正完成了消息的持久化。在这个过程中，如果消息还没有完成异步刷盘，RocketMQ中的Broker宕机的话，就会导致消息丢失
>   - 如果消息已经被刷入了磁盘中，但是数据没有做任何备份，一旦磁盘损坏，那么消息也会丢失
>
>   **解决：**
>
>   保证消息不丢失，首先需要将os cache的异步刷盘策略改为同步刷盘，这一步需要修改Broker的配置文件，将flushDiskType改为SYNC_FLUSH同步刷盘策略，默认的是ASYNC_FLUSH异步刷盘。
>
>   一旦同步刷盘返回成功，那么就一定保证消息已经持久化到磁盘中了；为了保证磁盘损坏不会丢失数据，我们需要对RocketMQ采用主从机构，集群部署，Leader中的数据在多个Follower中都存有备份，防止单点故障。
>
>   虽然我们可以通过配置的方式来达到MQ本身高可用的目的，但是都对性能有损耗，怎样配置需要根据业务做出权衡。
>
> - **消费者丢失**
>
>   **原因：**消费者成功从RocketMQ中获取到了消息，还没有将消息完全消费完的时候，就通知RocketMQ我已经将消息消费了，然后消费者宕机，但是RocketMQ认为消费者已经成功消费了数据，所以数据依旧丢失了。
>
>   **解决：**
>
>   RocketMQ默认是需要消费者回复ack确认。
>
>   ```java
>   //注册消息监听器处理消息
>   consumer.registerMessageListener(new MessageListenerConcurrently() {
>      @Override
>       public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context){                                  
>           //对消息进行处理
>           return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
>       }
>   });
>   ```
>
>   RocketMQ在消费者中注册了一个监听器，当消费者获取到了消息，就会去回调这个监听器函数，去处理里面的消息
>
>   当你的消息处理完毕之后，才会返回ConsumeConcurrentlyStatus.CONSUME_SUCCESS 只有返回了CONSUME_SUCCESS，消费者才会告诉RocketMQ我已经消费完了，此时如果消费者宕机，消息已经处理完了，也就不会丢失消息了
>
>   如果消费者还没有返回CONSUME_SUCCESS时就宕机了，那么RocketMQ就会认为你这个消费者节点挂掉了，会自动故障转移，将消息交给消费者组的其他消费者去消费这个消息，保证消息不会丢失。
>
>   消费方不返回ack确认，重发的机制根据MQ类型的不同发送时间间隔、次数都不尽相同，如果重试超过次数之后会进入死信队列，需要手工来处理了。

#### **消息堆积**

> **产生消息堆积的根源其实就只有两个——生产者生产太快或者消费者消费太慢。**
>
> 因为考虑到时消费者消费一直出错的问题，那么我们可以从以下几个角度来考虑：
>
> 1. 【生产者生产太快】
>
>    当流量到峰值的时候是因为生产者生产太快，我们可以使用一些 **限流降级** 的方法，当然你也可以增加多个消费者实例去水平扩展增加消费能力来匹配生产的激增。
>
> 2. 【消费者消费太慢】
>
>    消费者出错，肯定是程序或者其他问题导致的，如果容易修复，先把问题修复，让consumer恢复正常消费
>
>    如果时间来不及处理很麻烦，做转发处理，写一个临时的consumer消费方案，先把消息消费，然后再转发到一个新的topic和MQ资源，这个新的topic的机器资源单独申请，要能承载住当前积压的消息
>
>    处理完积压数据后，修复consumer，去消费新的MQ和现有的MQ数据，新MQ消费完成后恢复原状

#### **重复消费/消息幂等**

> **原因：**
>
> - produce发送到Broker时消息重复
>
>   当一条消息已被成功发送到服务端并完成持久化，此时出现了网络闪断或者producer宕机，导致服务端Broker 对 producer应答失败。 如果此时生产者意识到消息发送失败并尝试再次发送消息，消费者后续会收到两条内容相同并且 Message ID 也相同的消息。
>
> - Broker投递消息到Consumer时消息重复
>
>   消息消费的场景下，消息已投递到消费者并完成业务处理，当客户端给服务端反馈应答的时候网络闪断。 为了保证消息至少被消费一次，消息队列 RocketMQ 的服务端将在网络恢复后再次尝试投递之前已被处理过的消息，消费者后续会收到两条内容相同并且 Message ID 也相同的消息。
>
> **解决：**【幂等性解决方案】
>
> - 强校验
>
>   场景：如与金钱相关的支付等关键消息，必须强校验。
>
>   基于数据库的唯一键来保证重复数据不会被插入多条。建立一个已消费消息的表，每次消费之前检查消费表中当前消费的消息是否已经存在，若存在表示消息已经被消费过直接返回。
>
> - 弱校验
>   场景：可以有小概率出现重复消费的非关键消息
>
>   基于Redis的实现：
>
>   使用set结构实现。每次消费前查看set中是否已经存在待消费的消息的唯一标识符，不存在则消息，存在则直接返回。
>   场景唯一标识+id作为Redis的key，并设置一定的过期时间。每次消费时检查key是否已经存在，存在则直接返回

#### **顺序消费**

> 在上面的技术架构介绍中，我们已经知道了 **`RocketMQ` 在主题上是无序的、它只有在队列层面才是保证有序** 的。
>
> 这又扯到两个概念——**普通顺序** 和 **严格顺序** 。
>
> - 所谓普通顺序是指 消费者通过 **同一个消费队列收到的消息是有顺序的** ，不同消息队列收到的消息则可能是无顺序的。普通顺序消息在 `Broker` **重启情况下不会保证消息顺序性** (短暂时间) 。
> - 所谓严格顺序是指 消费者收到的 **所有消息** 均是有顺序的。严格顺序消息 **即使在异常情况下也会保证消息的顺序性** 。
>
> !>严格顺序看起来虽好，实现它可会付出巨大的代价。如果你使用严格顺序模式，`Broker` 集群中只要有一台机器不可用，则整个集群都不可用。
>
> 一般而言，我们的 `MQ` 都是能容忍短暂的乱序，所以推荐使用普通顺序模式。
>
> 其实很简单，我们需要处理的仅仅是将同一语义下的消息放入同一个队列(比如这里是同一个订单)，那我们就可以使用 **Hash取模法** 来保证同一个订单在同一个队列中就行了。

#### **分布式事务**

> 事务消息就是MQ提供的类似XA的分布式事务能力，通过事务消息可以达到分布式事务的最终一致性。
>
> 半事务消息就是MQ收到了生产者的消息，但是没有收到二次确认，不能投递的消息。
>
> 实现原理如下：
>
> 1. 生产者先发送一条半事务消息到MQ
>
> 2. MQ收到消息后返回ack确认
>
> 3. 生产者开始执行本地事务
>
> 4. 如果事务执行成功发送commit到MQ，失败发送rollback
>
> 5. 如果MQ长时间未收到生产者的二次确认commit或者rollback，MQ对生产者发起消息回查
>
> 6. 生产者查询事务执行最终状态
>
> 7. 根据查询事务状态再次提交二次确认
>
>
> 最终，如果MQ收到二次确认commit，就可以把消息投递给消费者，反之如果是rollback，消息会保存下来并且在3天后被删除。
>
> ![](rocketmq_imgs\7581ba66fe8c17baad15f89e27de00a4.png)

<!-- tabs:end -->

### 分析了 `RocketMQ` 的技术架构(`NameServer` 、`Broker` 、`Producer` 、`Comsumer`)

> | 角色       | 作用                                                         |
> | ---------- | ------------------------------------------------------------ |
> | Nameserver | 无状态，动态列表；这也是和zookeeper的重要区别之一。zookeeper是有状态的。 |
> | Producer   | 消息生产者，负责发消息到Broker。                             |
> | Broker     | 就是MQ本身，负责收发消息、持久化消息等。                     |
> | Consumer   | 消息消费者，负责从Broker上拉取消息进行消费，消费完进行ack。  |
>
> **【实现原理】说了这么多，那你说说RocketMQ实现原理吧？**
>
> RocketMQ由NameServer注册中心集群、Producer生产者集群、Consumer消费者集群和若干Broker（RocketMQ进程）组成，它的架构原理是这样的：
>
> 1. Broker在启动的时候去向所有的NameServer注册，并保持长连接，每30s发送一次心跳
>
> 2. Producer在发送消息的时候从NameServer获取Broker服务器地址，根据负载均衡算法选择一台服务器来发送消息
>
> 3. Conusmer消费消息的时候同样从NameServer获取Broker地址，然后主动拉取消息来消费
>

### 介绍了 `RocketMQ` 的存储机制和刷盘策略。⭐

?>`刷盘策略`

> **消息刷盘**
>
> (1) 同步刷盘：只有在消息真正持久化至磁盘后RocketMQ的Broker端才会真正返回给Producer端一个成功的ACK响应。同步刷盘对MQ消息可靠性来说是一种不错的保障，但是性能上会有较大影响，一般适用于金融业务应用该模式较多。
>
> (2) 异步刷盘：能够充分利用OS的PageCache的优势，只要消息写入PageCache即可将成功的ACK返回给Producer端。消息刷盘采用后台异步线程提交的方式进行，降低了读写延迟，提高了MQ的性能和吞吐量。
>
> 一般地，**异步刷盘只有在 `Broker` 意外宕机的时候会丢失部分数据**，你可以设置 `Broker` 的参数 `FlushDiskType` 来调整你的刷盘策略(ASYNC_FLUSH 或者 SYNC_FLUSH)。

?>`同步复制和异步复制`

> 上面的同步刷盘和异步刷盘是在单个结点层面的，而同步复制和异步复制主要是指的 `Borker` 主从模式下，主节点返回消息给客户端的时候是否需要同步从节点。
>
> - 同步复制： 也叫 “同步双写”，也就是说，
>
>   同步复制方式是等Master和Slave均写 成功后才反馈给客户端写成功状态；
>   在同步复制方式下，如果Master出故障，Slave上有全部的备份数据，容易恢复，但是同步复制会增大数据写入延迟，降低系统[吞吐量](https://so.csdn.net/so/search?q=吞吐量&spm=1001.2101.3001.7020)。
>
> - 异步复制：
>
>   异步复制方式是只要Master写成功 即可反馈给客户端写成功状态。
>   在异步复制方式下，系统拥有较低的延迟和较高的吞吐量，但是如果Master出了故障，有些数据因为没有被写 入Slave，有可能会丢失；
>
> 同步复制和异步复制是通过Broker配置文件里的brokerRole参数进行设置的，这个参数可以被设置成ASYNC_MASTER、 SYNC_MASTER、SLAVE三个值中的一个。

?>`存储机制`

> 在 `Topic` 中的 **队列是以什么样的形式存在的？队列中的消息又是如何进行存储持久化的呢？**
>
> - **「CommitLog」**：消息真正的存储文件，所有消息都存储在 CommitLog 文件中。
>
>   生产者将消息发送到 RocketMQ 的 Broker 后，Broker 服务器会将**「消息顺序写入到 CommitLog 文件中」**，这也就是 RocketMQ 高性能的原因，因为我们知道磁盘顺序写特别快，RocketMQ 充分利用了这一点，极大的提高消息写入效率。
>
> - **「ConsumeQueue」**：消息消费逻辑队列，类似数据库的索引文件。
>
>   `Consumer` 即可根据 `ConsumeQueue` 来查找待消费的消息。其中，`ConsumeQueue`（逻辑消费队列）**作为消费消息的索引**，保存了指定 `Topic` 下的队列消息在 `CommitLog` 中的**起始物理偏移量 `offset` \**，消息大小 `size` 和消息 `Tag` 的 `HashCode` 值。\**`consumequeue` 文件可以看成是基于 `topic` 的 `commitlog` 索引文件
>
> - **「IndexFile」**：消息索引文件，主要存储消息 Key 与 offset 对应关系，提升消息检索速度。

### Master和Slave之间是怎么同步数据的呢？

> 而消息在master和slave之间的同步是根据raft协议来进行的：
>
> 1. 在broker收到消息后，会被标记为uncommitted状态
>
> 2. 然后会把消息发送给所有的slave
>
> 3. slave在收到消息之后返回ack响应给master
>
> 4. master在收到超过半数的ack之后，把消息标记为committed
>
> 5. 发送committed消息给所有slave，slave也修改状态为committed
>



